﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoundTextManager : MonoBehaviour {

	int round;


	Text text;


	void Awake ()
	{
		text = GetComponent <Text> ();
		round = 1;
	}


	void Update ()
	{
		int round = EnemyManager.round;
		text.text = "Round " + round + " complete!\nRound " + (round + 1) + " is about to begin!";
	}
}
