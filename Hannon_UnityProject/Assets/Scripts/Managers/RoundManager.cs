﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManager : MonoBehaviour 
{
	PlayerHealth playerHealth;
	Animator anim;
	bool started = false;
	GameObject player;

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		anim = GetComponent<Animator>();
		playerHealth = player.GetComponent <PlayerHealth> ();
	}


	void Update()
	{	
		if (EnemyManager.left == 0 && started == false) {
			anim.SetTrigger ("beginTrans");
			StartCoroutine (Rounds());
			started = true;
		}
	}

	IEnumerator Rounds ()
	{
		yield return new WaitForSeconds (5);
		EnemyManager.round += 1;
		EnemyManager.numberSpawn = EnemyManager.round * 5;
		EnemyManager.left = EnemyManager.numberSpawn;
		playerHealth.currentHealth = 100;
		started = false;

	}
}

