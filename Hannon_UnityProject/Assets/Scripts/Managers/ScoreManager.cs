﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;


    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
    }


    void Update ()
    {
		int left = EnemyManager.left;
        text.text = "Enemies Left: " + left + "        Round: " + EnemyManager.round + "\nScore: " + score;
    }
}
