﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
	public static int round = 1;
	public static int numberSpawn = round*5;
	public static int left = numberSpawn;



    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime);

    }


    void Spawn ()
    {
        if(playerHealth.currentHealth <= 0f)
        {
            return;
        }

		if (numberSpawn < 1) {
			return;
		}

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		numberSpawn -= 1;

        Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
